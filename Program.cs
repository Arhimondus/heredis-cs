﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization;
using HandlebarsDotNet;
using System.Diagnostics.Contracts;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Net;
using System.Text;

namespace heredis
{
	public class MainClass
	{
		public static Deserializer Yaml = new DeserializerBuilder().Build();
		public static void Main(string[] args)
		{
			//Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo;
			//var use_server = (args.Length >= 1 && args[0] == "server");
			var use_server = args.Length > 0 && args[0] == "server";

			Handlebars.RegisterHelper("jsonify", (writer, context, parameters) => {
				//if (parameters == null || parameters.Length == 0) writer.Write("[]");
				writer.Write("[" + string.Join(",", (parameters[0] as List<object>).Cast<string>().Select(s => "\"" + s + "\"")) + "]");
			});

			Handlebars.RegisterHelper("index", (writer, context, parameters) => {
				var index = Convert.ToInt32(parameters[1]);
				var array = parameters[0] as List<Object>;
				writer.Write(array[index]);
			});

			Handlebars.RegisterHelper("contents", (writer, options, context, parameters) => {
				foreach (var content in (context.Contents as IEnumerable<CContent>).Where(w => w.Class == parameters[0].ToString() && w.UseSingle))
					options.Template(writer, content);
			});

			var site = Yaml.Deserialize<Site>(File.ReadAllText("site.yaml"));

			var contents = new List<CContent>();

			var contentFiles = Directory.GetFiles("content", "*.*", SearchOption.AllDirectories);
			var structure = new List<ContentClass>(); //root_classes
			foreach (var file in contentFiles)
			{
				var fullName = string.Join("\\", file.Split('\\').Skip(1).Reverse().Skip(1).Reverse());
				var pts = file.Replace("content\\", "public\\").Replace(".md", ".html");
				if (pts.EndsWith("_index.html")) pts = pts.Replace("_index.html", "index.html");
				else pts = pts.Replace(".html", "\\index.html");
				contents.Add(new CContent
				{
					RawContent = File.ReadAllText(file).Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n"),
					Class = fullName.Replace("\\", "/"),
					PathToSave = pts,
					Site = site,
					UseSingle = Path.GetFileNameWithoutExtension(file) != "_index"
				});
				if (!structure.Any(a => a.FullName == fullName))
				{
					var classPath = fullName.Split('\\');
					var name = classPath.Last();
					var contentClass = new ContentClass { Name = name, FullName = fullName };
					contentClass.RegenerateParents(structure);
				}
			}

			var partials = new Dictionary<string, string>();
			var partialFiles = Directory.GetFiles(Path.Combine("layouts", "partials"), "*.html", SearchOption.AllDirectories);
			foreach (var file in partialFiles)
			{
				var fileContent = File.ReadAllText(file);
				var partialTemplateName = string.Join("\\", file.Split('\\').Skip(2));
				Handlebars.RegisterTemplate(partialTemplateName, fileContent);
			}

			Func<object, string> @defaultSingle = null;
			var path = Path.Combine("layouts", "_default", "single.html");
			if (File.Exists(path))
				@defaultSingle = Handlebars.Compile(File.ReadAllText(path));

			Func<object, string> @defaultList = null;
			path = Path.Combine("layouts", "_default", "single.html");
			if (File.Exists(path))
			@defaultList = Handlebars.Compile(File.ReadAllText(path));
			
			//Считываем конфиги из структуры
			foreach (var @class in structure)
			{
				var pathSingle = Path.Combine("layouts", @class.FullName, "single.html");
				var pathList = Path.Combine("layouts", @class.FullName, "list.html");
				if (File.Exists(pathSingle))
					@class.LayoutSingle = Handlebars.Compile(File.ReadAllText(pathSingle));
				if (File.Exists(pathList))
					@class.LayoutList = Handlebars.Compile(File.ReadAllText(pathList));
				if (@class.LayoutSingle == null) 
				{
					if (@defaultSingle != null)
						@class.LayoutSingle = @defaultSingle;
					else
						@class.LayoutSingle = Handlebars.Compile("");
				}
				if (@class.LayoutList == null) 
				{
					if (@defaultList != null)
						@class.LayoutList = @defaultList;
					else
						@class.LayoutList = Handlebars.Compile("");
				}
			}


			foreach (var content in contents)
				content.PreRender(structure);
			foreach (var content in contents)
			{
				content.Render2(contents);
				if(!use_server)
					content.Save();
			}

			contents = contents.OrderByDescending(o => o.VirtualPath.Length).ToList();

			if (!use_server)
				foreach (var file in Directory.GetFiles("static", "*.*", SearchOption.AllDirectories))
				{
					var dest = file.Replace("static\\", "public\\");
					var destFolder = Path.GetDirectoryName(dest);
					if (!Directory.Exists(destFolder)) Directory.CreateDirectory(destFolder);
					File.Copy(file, dest, true);
				}

			if (use_server)
			{
				var server = new HttpListener();
				if (!HttpListener.IsSupported) return;
				server.Prefixes.Add("http://localhost:1234/");
				server.Start();
				while (server.IsListening)
				{
					HttpListenerContext context = server.GetContext();
					HttpListenerRequest request = context.Request;
					HttpListenerResponse response = context.Response;
					if (request.Url.LocalPath.EndsWith(".png") ||
					   request.Url.LocalPath.EndsWith(".jpg") ||
					   request.Url.LocalPath.EndsWith(".css") ||
					   request.Url.LocalPath.EndsWith(".js") ||
					   request.Url.LocalPath.EndsWith(".ico"))
					{
						if (File.Exists("static" + request.Url.LocalPath))
						{
							var buffer = File.ReadAllBytes("static" + request.Url.LocalPath);
							using (Stream output = response.OutputStream)
							{
								output.Write(buffer, 0, buffer.Length);
							}
						}
						else
						{
							byte[] buffer = Encoding.UTF8.GetBytes("404");
							using (Stream output = response.OutputStream)
							{
								response.StatusCode = 404;
								output.Write(buffer, 0, buffer.Length);
							}
						}
						continue;
					}

					var contentPage = contents.FirstOrDefault(f => f.VirtualPath == request.Url.LocalPath);
					if (contentPage != null)
					{
						response.ContentType = "text/html;charset=utf-8";
						WriteString(response, contentPage.RenderedContent);
						response.Close();
					}
					else
					{
						byte[] buffer = Encoding.UTF8.GetBytes("404");
						using (Stream output = response.OutputStream)
						{
							response.StatusCode = 404;
							output.Write(buffer, 0, buffer.Length);
						}
					}
				}
			}
		}

		public static int WriteString(HttpListenerResponse response, string message, int offset = 0)
		{
			var buffer = Encoding.UTF8.GetBytes(message);
			response.OutputStream.Write(buffer, offset, buffer.Length);
			return buffer.Length;
		}
	}

	public class Site 
	{
		public string baseUrl { get; set; }
		public Dictionary<string, string> @params { get; set; }
	}

	public class CContent
	{
		public string RawContent = "";
		public string TextContent;
		public Dictionary<string, dynamic> Params;
		public string Class;
		public string PathToSave;
		public Site Site;
		public string URL { get { return VirtualPath; } }
		public string RenderedContent;
		public bool UseSingle;
		public List<ContentClass> Structure;
		public List<CContent> Contents;
		public string Content { 
			get 
			{
				return CommonMark.CommonMarkConverter.Convert(TextContent);	
			}
		}
		public string VirtualPath {
			get {
				var vp = PathToSave.Replace("public", "").Replace("\\", "/").Replace("/index.html", "");
				return vp.Length == 0 ? "/" : vp;
			}
		}
		public void PreRender(IEnumerable<ContentClass> structure)
		{
			if (RawContent.StartsWith("+++"))
			{
				var index = RawContent.IndexOf(Environment.NewLine + "+++", 3);
				if (index > 0)
				{
					Params = MainClass.Yaml.Deserialize<Dictionary<string, dynamic>>(RawContent.Substring(3 + Environment.NewLine.Length, index - 3 - Environment.NewLine.Length));
					TextContent = RawContent.Substring(index + 5);
				}
			}
			else
				TextContent = RawContent;
			Structure = structure.ToList();
		}
		public void Render2(List<CContent> contents)
		{
			Contents = contents;
			var @class = Structure.First(f => f.FullName.Replace("\\", "/") == Class);

			if(UseSingle)
				RenderedContent = RepairUnicode(@class.LayoutSingle(this));
			else
				RenderedContent = RepairUnicode(@class.LayoutList(this));
		}
		public void Save()
		{
			Directory.CreateDirectory(Path.GetDirectoryName(PathToSave));
			File.WriteAllText(PathToSave, RenderedContent);
		}
		private string RepairUnicode(string u)
		{
			return Regex.Replace(u, @"&#([0-9]+);", (match) =>
			{
				return char.ConvertFromUtf32(Convert.ToInt32(match.Groups[1].Value));
			});
		}
	}

	public class ContentClass
	{
		public string Name;
		public string FullName;
		public ContentClass Parent;
		public List<ContentClass> Childs = new List<ContentClass>();
		public Func<object, string> LayoutSingle;
		public Func<object, string> LayoutList;
		public void RegenerateParents(List<ContentClass> structure)
		{
			structure.Add(this);
			var classPath = FullName.Split('\\');
			for (var i = classPath.Length - 1; i > 0; i--)
			{
				var parentFullName = String.Join("\\", classPath.Take(i));
				var parent = structure.FirstOrDefault(f => f.FullName == parentFullName);
				if (parent == null)
				{
					parent = new ContentClass { Name = classPath.Last(), FullName = parentFullName };
					parent.RegenerateParents(structure);
				}
                this.Parent = parent;
				this.Parent.Childs.Add(this);                
			}
		}
		public override string ToString()
		{
			return FullName;
		}
	}
}
